##########################

# MAIN CONFIGURATION FILE
class1="./images/simplicity/class1/training"
class2="./images/simplicity/class2/training"
class3="./images/simplicity/class3/training"
class4="./images/simplicity/class4/training"
class5="./images/simplicity/class5/training"
class6="./images/simplicity/class6/training"
class7="./images/simplicity/class7/training"
class8="./images/simplicity/class8/training"
class9="./images/simplicity/class9/training"
class10="./images/simplicity/class10/training"

test1="./images/simplicity/class1/validation"
test2="./images/simplicity/class2/validation"
test3="./images/simplicity/class3/validation"
test4="./images/simplicity/class4/validation"
test5="./images/simplicity/class5/validation"
test6="./images/simplicity/class6/validation"
test7="./images/simplicity/class7/validation"
test8="./images/simplicity/class8/validation"
test9="./images/simplicity/class9/validation"
test10="./images/simplicity/class10/validation"


#PATH="./training"

SAMPL = 300 # number of sampled keycodes

# use PCA to compress sift keys
do_pca = 'no'

# number of PCA dimensions
dpca = 60

# cluster the sift features of each image to compress it
cluster1 = 'no' # yes or no

# cluster all the images of the same class together
cluster2 = 'yes' # yes or no

# number of K clusters in K-means
K = 1000 # number of clustering
K2 = 200 # number of possible 'words' for dictionary

# number of initial iterations on k-means
kiter = 4

# SVM Configuration

# kernel type
kernel = 'LINEAR' 

##########################

# print the paramaters

print ' MAIN CONFIGURATION '

print 'Training classes'
print class1
print class2

print 'Validation classes'
print test1
print test2

print 'PCA Parameter'
print dpca

#PATH="./training"

print 'number of samples'
print SAMPL # number of sampled keycodes

# cluster the sift features of each image to compress it
print 'first clustering'
print cluster1 # yes or no

print 'second clustering'
# cluster all the images of the same class together
print cluster2 # yes or no

# number of K clusters in K-means
print 'number cluster in k-means'
print K  # number of clustering
print 'number of words in dictionary'
print K2  # number of possible 'words' for dictionary
whitening = 'no'
print '######################'
