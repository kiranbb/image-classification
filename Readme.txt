Python code to implement image-classification using sift and k-means.
Lead Author : Oriel Frigo (oriel.frigo@gmail.com)
Co-Author : Kiran Bylappa Raja (kiran.bb@gmail.com)
