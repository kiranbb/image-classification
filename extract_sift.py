import os
from os import listdir, system
from conf import *
from util import *
#from bow import *

# run Lowe's SIFT binary and write the output in .key files
def extract_features(dir):
    files = listdir(dir)
    for f in files:
        filename = "%s/%s" % (dir, f)
        [name, ext] = fileinfo(filename)
        if ext == '.pgm':
            system("./sift < %s > %s" % (filename, name + ".key"))


# read one .key file to get the SIFT features
def load_feature_file(feature_file):
    file_features = []
    f = open(feature_file, 'r')
    (num_features, num_dimensions) = map(int, f.readline().split())

    for i in range(0, num_features):
        #(row, col, scale, orientation) = map(float, f.readline().split())
        rcso = map(float, f.readline().split())
        
        feature = []
        for j in range(0, 7):
            map(feature.append, map(float, f.readline().split()))
        file_features.append(tuple(feature))
    #print file_features
    return file_features


# return a list of features of all images in the class dir
def load_features(dir):
    files = listdir(dir)
    features = []
    for f in files:
        filename = "%s/%s" % (dir, f)
        if os.path.isfile(filename) and os.path.splitext(filename)[1].lower() == '.key':
            print 'getting features from %s' % filename
            features = load_feature_file(filename)
            #extract_clusters(features, filename)
            print 'random_sampling'
            [name, ext] = fileinfo(filename)
            samplefile = "%s.sam" % (name)
            sampled = random_sampling(features)
            write_samples(samplefile, sampled)

    return features


def write_whitened_features(dir,wff):
    files = listdir(dir)
    features = []
    for f in files:
        filename = "%s/%s" % (dir, f)
        if os.path.isfile(filename) and os.path.splitext(filename)[1].lower() == '.key':
            [name, ext] = fileinfo(filename)
            whitefile = "%s.whi" % (name)
            write_white(whitefile, wff[0])
            wff = wff[1:]

    return wff

def write_white(filename, samples):
    f = open(filename, 'w')
    for i in range(0, len(samples)):
        for j in range(0, len(samples[i])):
            f.write("%d " % samples[i][j])
        f.write("\n")
    f.close()

#from bow import extract_clusters
# return a list of tuples containing samples of class dir [(filename, sampledfeatures)]
def load_sampled_features(dir):
    samples = []
    files = listdir(dir)
    for f in files:
        filename = "%s/%s" % (dir, f)
        [name, ext] = fileinfo(filename)
        if ext == '.sam':
            f = open(filename, 'r')
            features = []
            for l in f:
                features.append(map(int, l.split()))
            samples.append((name, features))                                                                                   
    return samples

