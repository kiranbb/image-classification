from conf import *
import math
import sys
import os
from os import listdir, system
from time import time
from newkmeans import *
from cluster import * 
from util import *
from numpy import array
import scipy
from scipy.cluster.vq import vq, kmeans, whiten

def load_sampled_features(dir):
    samples = []
    files = listdir(dir)
    for f in files:
        filename = "%s/%s" % (dir, f)
        [name, ext] = fileinfo(filename)
        if ext == '.sam':
            f = open(filename, 'r')
            features = []
            for l in f:
                features.append(map(int, l.split()))
            samples.append((name, features))                                                                                   
    return samples


def extract_clusters(features, filename):
    print "Generating clusters for each image..."
    print 'length of features'
    print len(features)
    print 'dimension of each feature'

    size = len(features[0])
    print size
    
    # IMPORTANT - take random samples from SIFT feature space

    sampled = random_sampling(features) 
    #sampled = fixed_sampling(features)

    #cl = KMeansClustering(sampled)
    #compressed_features = cl.getclusters(K)

    print 'length of clustered features'
    print len(compressed_features)

    [name, ext] = fileinfo(filename)
    clusterfile = "%s.clu" % (name)
    samplefile = "%s.sam" % (name)

    write_samples(samplefile, sampled)

    # take only the centroid to write in the files
    #mclusters = []
    #for i in range(0, len(compressed_features)):
    #    mcentroid = centroid(compressed_features[i])
    #    mclusters.append(mcentroid)
    mclusters = fastkmeans(features, K)
    write_cluster(clusterfile, mclusters)

# this is in the case of clustering all the images for the entire class
def extract_cluster_from_clusters(features, clusterfile):
    stime = time()
    #cl = KMeansClustering(features)
    #compressed_features = cl.getclusters(K)

    print 'clustering everything..............'
    #print len(compressed_features)
    

    #if os.path.isfile(clusterfile) == False:
    mclusters = fastkmeans(array(features), K2)

    # take only the centroid to write in the files
    #mclusters = []
    #for i in range(0, len(compressed_features)):
    #    mcentroid = centroid(compressed_features[i])
    #    mclusters.append(mcentroid)
    
    ftime = time()
    print 'total processing time for cluster: %s' % str(ftime-stime)                                                  

    write_cluster(clusterfile, mclusters)

    #else:
	#print 'empty cluster' 
	#mclusters = [] # TODO write a function to load .cl2
    # return the main dictionary for BOW
    return mclusters

# load .clu clusters or .sam features from this class  
def load_clusters(dir):
    clusters = []
    files = os.listdir(dir)
    features = []

    if cluster1 == 'yes':
        source = '.clu'
    else:
        source = '.sam'

    for f in files:
        filename = "%s/%s" % (dir, f)
        [name, ext] = fileinfo(filename)
        if os.path.isfile(filename) and ext == source:
            f = open(filename, 'r')
            print filename
            for l in f:
                clusters.append(map(int, l.split()))

    #clustertuple = map(tuple, clusters)
    #clustertuple = clusters    
    return clusters

# generate a .cl2 that is a cluster of all classes together
def generate_dictionary(dir, clusters):
    if cluster2 == 'yes':

	clusterfile = "%s/dictionary.cl2" % (dir)
        # print clusters
        # print 'len of clusters'
        # print len(clusters)
        classcluster = extract_cluster_from_clusters(clusters, clusterfile)
        return classcluster
    
    else: return clusters


# load the .cl2 dictionary 
def load_dictionary(dir):
    clusters = []
    filename = "%s/dictionary.cl2" % (dir)

    if os.path.isfile(filename):	
    	f = open(filename, 'r')
    	for l in f: clusters.append(map(int, l.split()))
	f.close()

    return clusters


def save_clusters():
    f = open("clusters.txt", 'w')    
    for i in range(0, len(clusters)):
        for j in range(0, len(clusters[i])):
            f.write("%d " % clusters[i][j])
        f.write("\n")


def write_cluster(filename, clusters):
    f = open(filename, 'w')
    
    for i in range(0, len(clusters)):
        for j in range(0, len(clusters[i])):
            f.write("%d " % clusters[i][j])
        f.write("\n")
    f.close()



def save_vector(filename, vector):
    f = open(filename, 'w')
    
    for i in range(0, len(vector)):
        f.write("%d " % vector[i])



# euclidian distance between two vectors
def pyth(vector1, vector2):
    sum = 0
    
    for i in range(0, len(vector1)):
        sum += (vector1[i] - vector2[i])**2

    return math.sqrt(sum)

def find_cluster(feature, clusters):
    # Loop through all clusters, find the closest
    min_dist = None
    cluster = None

    # print 'this length of clusters'
    # print len(clusters)
    
    for i in range(0, len(clusters)):
        dist = pyth(feature, clusters[i])

        if min_dist is None or dist < min_dist:
            min_dist = dist
            cluster = i

    return cluster

from scipy.cluster.vq import whiten
from extract_sift import load_feature_file
def generate_bow(dir, clusters):
    stime = time()
    # For each image in the directory:
    files = os.listdir(dir)
    
    # count the number of sampled feature files
    fcount = 0
    for x in files:
       if(x.find('.sam') > -1): fcount=fcount+1

    # load the samples from all the images
    file_features = load_sampled_features(dir)
    #file_features = load_whitened_features(dir)
    
    # whitening verything together:
    # whitef = []
    # for x in file_features: whitef += x[1]
    # whitef = whiten(array(whitef))
    # print len(whitef)
    # raw_input()
    # i=0

    # approach using SAMPLED features to construct histogram         
     
    # For each of its SIFT sampled features
    for (name, features) in file_features:
    #    
	print 'generating bow for %s' % name
        if cluster2 == 'yes':
            vector = [0] * K2

            if whitening == 'yes':
                features = whiten(array(features)) 
                features = features.tolist()
            
        else: 
            vector = [0] * len(clusters) * fcount 

        #for key in features:
        # #Find the cluster closest to it
             #cluster = find_cluster(key, clusters)
        #     #vq(obs,code_book)
            
        #     #Increment that count in its vector
             #vector[cluster] = vector[cluster] + 1
        #print len(features)
        #print len(clusters)
        [obs,dist] = vq((array(features)),array(clusters))
        # i = i + 1
        for o in obs:
             vector[o] = vector[o] + 1

        bowfile = "%s.bow" % (name)
        save_vector(bowfile, vector)
             
    # end the approach

    # approach using all the sift keys to construct histogram
    # for f in files:
    #     filename = "%s/%s" % (dir, f)
    #     [name, ext] = fileinfo(f)
            
    #     if ext == '.sam':
    #         print 'generating bow for %s' % name
    #         keyfile = "%s/%s.key" % (dir, name)
    #         feats = load_feature_file(keyfile)       
    #         if cluster2 == 'yes':
    #             vector = [0] * K2 

    #         else: 
    #             vector = [0] * len(clusters) * fcount

            # for key in feats:
            # # Find the cluster closest to it
            #     cluster = find_cluster(key, clusters)
            # # Increment that count in its vector
            #     vector[cluster] = vector[cluster] + 1    

            #[vector,dist] = vq(array(feats),array(clusters))
            # [obs,dist] = vq(whiten(array(feats)),array(clusters))
            # for o in obs:
            #     vector[o] = vector[o] + 1


            # bowfile = "%s/%s.bow" % (dir, name)                
            # save_vector(bowfile, vector)
    

    ftime = time()
    print 'total processing time for bow: %s' % str(ftime-stime)                 


def load_whitened_features(dir):
    samples = []
    files = listdir(dir)
    for f in files:
        filename = "%s/%s" % (dir, f)
        [name, ext] = fileinfo(filename)
        if ext == '.whi':
            f = open(filename, 'r')
            features = []
            for l in f:
                features.append(map(int, l.split()))
            samples.append((name, features))                                                                                   
    return samples
