import scipy
import numpy
from numpy import array
from scipy.cluster.vq import vq, kmeans, whiten
from conf import *

def fastkmeans(features, codes):
	# whitened = whiten(features)
	# book = array((whitened[0],whitened[2]))
	clusters = kmeans(features,codes, iter=kiter)
	return clusters[0].tolist()

