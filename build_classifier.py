# helper functions
from extract_sift import *
from bow import *
from train_svm import *
from util import *
import conf
from numpy import shape
# convert all the images to pgm and put in the class folders
#xml_labels("./VOC2011/JPEGImages", "./VOC2011/Annotations")
    

#raw_input()
# Extract SIFT features for all images

print 'extracting features'
# extract_features(class1)
# extract_features(class2)
# extract_features(class3)
# extract_features(class4)
# extract_features(class5)
# extract_features(class6)
# extract_features(class7)
# extract_features(class8)
# extract_features(class9)
# extract_features(class10)

# extract_features(test1)
# extract_features(test2)
# extract_features(test3)
# extract_features(test4)
# extract_features(test5)
# extract_features(test6)
# extract_features(test7)
# extract_features(test8)
# extract_features(test9)
# extract_features(test10)

load_features(class1)
load_features(class2)
load_features(class3)
load_features(class4)
load_features(class5)
load_features(class6)
load_features(class7)
load_features(class8)
load_features(class9)
load_features(class10)

load_features(test1)
load_features(test2)
load_features(test3)
load_features(test4)
load_features(test5)
load_features(test6)
load_features(test7)
load_features(test8)
load_features(test9)
load_features(test10)

#specific class clusters
#maincluster = load_dictionary(class1)
maincluster = []
# if maincluster == []:
#         print 'maincluster dictionary is empty, need to generate it'
# 	kclusters = load_clusters(class1)
# 	mclusters = load_clusters(class2)        
# 	clusters = kclusters+mclusters

	#specific class clusters
#maincluster = load_dictionary(class1)
if maincluster == []:
	print 'maincluster dictionary is empty, need to generate it'
	clusters = load_clusters(class1)
	clusters += load_clusters(class2)
	clusters += load_clusters(class3)
	clusters += load_clusters(class4)        
	clusters += load_clusters(class5)
	clusters += load_clusters(class6)
	clusters += load_clusters(class7)
	clusters += load_clusters(class8)
	clusters += load_clusters(class9)
	clusters += load_clusters(class5)
	maincluster = generate_dictionary(class1, clusters)

maincluster = load_dictionary(class1)

# Generate bag-of-word vectors for each image
print "Generating training class bow."
generate_bow(class1, maincluster)
generate_bow(class2, maincluster)
generate_bow(class3, maincluster)
generate_bow(class4, maincluster)
generate_bow(class5, maincluster)
generate_bow(class6, maincluster)
generate_bow(class7, maincluster)
generate_bow(class8, maincluster)
generate_bow(class9, maincluster)
generate_bow(class10, maincluster)

# print 'Generate test class bow'
generate_bow(test1, maincluster)
generate_bow(test2, maincluster)
generate_bow(test3, maincluster)
generate_bow(test4, maincluster)
generate_bow(test5, maincluster)
generate_bow(test6, maincluster)
generate_bow(test7, maincluster)
generate_bow(test8, maincluster)
generate_bow(test9, maincluster)
generate_bow(test10, maincluster)

# kclusters = load_clusters(class1)
# mclusters = load_clusters(class2)        
# clusters = kclusters+mclusters
# #print len(clusters)
# #print clusters
# #raw_input()

# # write the whitened features:
# wclusters = whiten(array(clusters))
# wclusters = wclusters.tolist()

# #divide whitened features in the files
# wff = []
# j = 0
# for i in range(0,len(wclusters)/SAMPL+1):
#         wff.append((wclusters[j:j+SAMPL]))
#         j = j + SAMPL

# wff = write_whitened_features(class1,wff)
# wff = write_whitened_features(class2,wff)

#Generate bag-of-word vectors for each image
# print "Generating bow vectors for class 1.."
# generate_bow(class1, maincluster)
# print "Generating bow vectors for class 2.."
# generate_bow(class2, maincluster)

train_images()
