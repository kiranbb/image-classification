import math
import sys
import os

from svm import *
from svmutil import *

class1="./images/cars/pgm/training"
class2="./images/bike/pgm/training"

test1="./images/cars/pgm/training/val"
test2="./images/bike/pgm/training/val"

def load_bow(dir):
    vectors = []
    
    files = os.listdir(dir)
    for f in files:
        filename = "%s/%s" % (dir, f)

        if os.path.isfile(filename) and os.path.splitext(filename)[1].lower() == '.bow':
	    print filename
            f_h = open(filename, 'r')
            vectors.append(map(int, f_h.readline().split()))

    return vectors

# number of test set features
test = 30	

def train_images():
    kbow1 = load_bow(class1)
    mbow1 = load_bow(class2)

    # remove the test set from the features
    kbow = kbow1[0:(len(kbow1)-test)]
    mbow = mbow1[0:(len(mbow1)-test)]

    klabels = [0]*(len(kbow1)-test)
    mlabels = [1]*(len(mbow1)-test)
    labels = klabels+mlabels
    data_bow = kbow+mbow
    print labels
    print len(kbow)
    print len(kbow) - test
    print len(data_bow)
    #print "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"
    #print data_bow

    # print 'len of labels'
    # print labels
    # print len(labels)
    # print 'kbow'
    # print (kbow)
    # print 'mbow'
    # print (mbow)

    prob = svm_problem(labels, data_bow)
    #param = svm_parameter(kernel_type = LINEAR, c = 10)
    param = svm_parameter()
    param.kernel_type=LINEAR

    ## training  the model
    model = svm_train(prob, param)

    # generate the test features
    kbow = kbow1[len(kbow1)-test:len(kbow1)]
    mbow = mbow1[len(mbow1)-test:len(mbow1)]    

    klabels = [0]*((test))
    mlabels = [1]*((test))
    labels = klabels+mlabels

    print len(labels)
    print len(kbow+mbow)

    p_labels, p_acc, p_vals = svm_predict(labels, kbow+mbow, model)

    print p_labels
    print p_acc

train_images()
