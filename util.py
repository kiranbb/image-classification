from conf import *
from numpy import array
from random import randint
import os
import mdp
#SAMPL = 350 # number of sampled keycodes

def fileinfo(filename):
    ext = filename[-4:-1] + filename[-1]
    name = filename.replace(ext, '')
    return [name, ext]

def random_sampling(features):
    sampled = []
    n = len(features)
    selected = []
    i=0 
    if n < SAMPL:
	S = n
    else: S = SAMPL				   
    while i < S:
        rindex = randint(0, n-1)
        if rindex not in selected:
            selected.append(rindex)
            sampled.append(features[rindex])
	    i=i+1
    
    return sampled

def fixed_sampling(features):
    return features[0:S-1]

def write_samples(filename, samples):
    if do_pca == 'yes':
        pca = mdp.nodes.PCANode(output_dim=dpca)
        print 'performing PCA on %s' % filename
        pca.train(array(samples))
        out1 = pca(array(samples))
        samples = out1.tolist()
    f = open(filename, 'w')
    for i in range(0, len(samples)):
        for j in range(0, len(samples[i])):
            f.write("%d " % samples[i][j])
        f.write("\n")
    f.close()


from xml.dom.minidom import parseString

# take classes information and convert jpg to pgm
def xml_labels(jpegdir,xmldir):
    files = os.listdir(xmldir)
    labels = []

    for f in files:
        filename = "%s/%s" % (xmldir, f)
        [name, ext] = fileinfo(filename)
        print filename
        if ext == '.xml':
            file = open(filename,'r')
            data = file.read()
            file.close()
            dom = parseString(data)
            xmlTag = dom.getElementsByTagName('name')[0].toxml()
            xmlData=xmlTag.replace('<name>','').replace('</name>','')
            label = xmlData
            print label
            os.system("cd %s" % jpegdir)
            os.system("sh jpegtopgm.sh %s.jpg %s" % (name, label))
            
            
            labels.append(xmlData)
            
    return labels
